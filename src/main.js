import { createApp } from "vue";
import PrimeVue from "primevue/config";
import Lara from "@/presets/lara";
import "./style.css";
import App from "./App.vue";
import Toast from "primevue/toast";
import ToastService from "primevue/toastservice";

const app = createApp(App);

app.use(PrimeVue, {
  unstyled: true,
  pt: Lara, //apply preset
});
app.use(ToastService);
app.component("Toast", Toast);
app.mount("#app");
